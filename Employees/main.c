#include <sqlite3.h>
#include <stdio.h>
#include <string.h>


int callback(void *, int, char **, char **);

int selectFunc(int rc, char *err_msg, sqlite3 *db);

int insertFunc(int rc, char *err_msg, sqlite3 *db);

int deleteFunc(int rc, char *err_msg, sqlite3 *db);

int insertImageFunc(int rc, char *err_msg, sqlite3 *db);

int readImageFunc(int rc, char *err_msg, sqlite3 *db);

int parameterizedFunc(int rc, char *err_msg, sqlite3 *db, int type, int id, char *name);

int selectWithCharParamFunc(int rc, char *err_msg, sqlite3 *db, int type, char *name);

int menu(int rc, char *err_msg, sqlite3 *db);


int main(void) {
    //By default autocommit so don't need to change it
    
    //if we need transactions
    // "BEGIN TRANSACTION;"
    //...
    //"COMMIT;"
    
    sqlite3 *db;
    char *err_msg = 0;
    
    int rc = sqlite3_open("employees.db", &db);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n",
                sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return 1;
    }
    
//Select
//    if(selectFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
    
    
//Insert
//    if(insertFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
    
//Delete
//    if(deleteFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
//
    
//InserImageFunc
//    if(insertImageFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
    
//ReadImage
//    if(readImageFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
    
//ParameterizedQueries
//    if(parameterizedFunc(rc, err_msg, db) == 1) {
//        return 1;
//    }
    
    
//    int x = 0;
//    while(x == 0) {
//        x = menu(rc, err_msg, db);
//    }

    
    sqlite3_close(db);
    return 0;
}


int selectWithCharParamFunc(int rc, char *err_msg, sqlite3 *db, int type, char *name) {
    char *sql;
    
    if(type == 1) {
        sql = "SELECT * FROM emp WHERE LASTNAME =";
        strcat(sql, name);
    }
    
    if(type == 2) {
        sql = "SELECT * FROM emp WHERE COUNRY =";
        strcat(sql, name);
    }
    
    rc = sqlite3_exec(db, sql, callback, 0, &err_msg);
    
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "Failed to select data\n");
        fprintf(stderr, "SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
        sqlite3_close(db);
        
        return 1;
    }
    
    return 0;
}
//Use it for select by id
int parameterizedFunc(int rc, char *err_msg, sqlite3 *db, int type, int id, char *name){

    sqlite3_stmt *res = NULL;

    char *sql = "SELECT ID, FIRSTNAME, LASTNAME, BIRTHDATA, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA FROM emp WHERE Id = ?";
    rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (rc == SQLITE_OK) {
        if(type == 0) {
            sqlite3_bind_int(res, 1, id);  //set id param here
        }
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    int step = sqlite3_step(res);
    
    if (step == SQLITE_ROW) {
        printf("%s: ", sqlite3_column_text(res, 0));
        printf("%s\n", sqlite3_column_text(res, 1));
        printf("%s\n", sqlite3_column_text(res, 2));
        printf("%s\n", sqlite3_column_text(res, 3));
        printf("%s\n", sqlite3_column_text(res, 4));
        printf("%s\n", sqlite3_column_text(res, 5));
        printf("%s\n", sqlite3_column_text(res, 6));
        printf("%s\n", sqlite3_column_text(res, 7));
        printf("%s\n", sqlite3_column_text(res, 8));
        printf("%s\n", sqlite3_column_text(res, 9));
    }
    
    sqlite3_finalize(res);
    
    return 0;
}


int menu(int rc, char *err_msg, sqlite3 *db)
{
    char ch;
    char *mes;
    int exit = 0;
    
    printf ("%s\n", "I - Choose by ID");
    printf ("%s\n", "L - Choose by LASTNAME");
    printf ("%s\n", "C - Choose by COUNTRY");
    printf ("%s\n", "e - Exit");
    
    scanf("%c", &ch);
    
    printf ("%s\n", "--------------");
    
    switch(ch) {
        case 'I': {
            printf ("%s\n", "Choose by ID");
            int id;
            scanf("%d", &id);
            parameterizedFunc(rc, err_msg, db, 0, id, mes);
        }
            break;
            
        case 'L': {
            printf ("%s\n", "Choose by LASTNAME");
            char *lastName;
            scanf("%s", lastName);
            selectWithCharParamFunc(rc, err_msg, db, 1, lastName);
            
        }
            break;
            
        case 'C': {
            printf ("%s\n", "Choose by COUNTRY");
            char *country;
            scanf("%s", country);
            selectWithCharParamFunc(rc, err_msg, db, 2, country);
        }
            break;
            
        case 'e': {
            printf ("%s\n", "Exit");
            exit = 1;
        }
            break;
    }
    getchar();
    printf ("%s\n", "--------------");
    
    if(exit == 1)
        return 1;
    else 
        return 0;
}


int readImageFunc(int rc, char *err_msg, sqlite3 *db){
    FILE *fp = fopen("read.jpg", "wb");
    
    if (fp == NULL) {
        
        fprintf(stderr, "Cannot open image file\n");
        
        return 1;
    }
    
    char *sql = "SELECT PHOTO FROM emp WHERE Id = 1";
    
    sqlite3_stmt *pStmt;
    rc = sqlite3_prepare_v2(db, sql, -1, &pStmt, 0);
    
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "Failed to prepare statement\n");
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        
        sqlite3_close(db);
        
        return 1;
    }
    
    rc = sqlite3_step(pStmt);
    
    int bytes = 0;
    
    if (rc == SQLITE_ROW) {
        
        bytes = sqlite3_column_bytes(pStmt, 0);
    }
    
    fwrite(sqlite3_column_blob(pStmt, 0), bytes, 1, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fwrite() failed\n");
        
        return 1;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    rc = sqlite3_finalize(pStmt);
    
    return 0;
}


int insertImageFunc(int rc, char *err_msg, sqlite3 *db){
    FILE *fp = fopen("Ira.jpg", "rb");
    
    if (fp == NULL) {
        
        fprintf(stderr, "Cannot open image file\n");
        
        return 1;
    }
    
    fseek(fp, 0, SEEK_END);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    int flen = ftell(fp);
    
    if (flen == -1) {
        
        perror("error occurred");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    fseek(fp, 0, SEEK_SET);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    char data[flen+1];
    
    int size = fread(data, 1, flen, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fread() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    
    sqlite3_stmt *pStmt;
    
    char *sql = "INSERT INTO emp(PHOTO) VALUES(?)";
    
    rc = sqlite3_prepare(db, sql, -1, &pStmt, 0);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot prepare statement: %s\n", sqlite3_errmsg(db));
        
        return 1;
    }
    
    sqlite3_bind_blob(pStmt, 1, data, size, SQLITE_STATIC);
    
    rc = sqlite3_step(pStmt);
    
    if (rc != SQLITE_DONE) {
        
        printf("execution failed: %s", sqlite3_errmsg(db));
    }
    
    sqlite3_finalize(pStmt);

    return 0;
}


int deleteFunc(int rc, char *err_msg, sqlite3 *db){
    char *sql = "DELETE FROM emp WHERE ID = 7";
    rc =sqlite3_exec(db, sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
        sqlite3_close(db);
        
        return 1;
    }
    
    return 0;
}


int insertFunc(int rc, char *err_msg, sqlite3 *db) {
    char *sql = "INSERT INTO emp(ID, FIRSTNAME, LASTNAME, BIRTHDATA, PHOTO, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA) VALUES( 7, 'FirstName', 'LastName', 'BirthData', 'NULL', 'City', 'County', 'Address', 'Department', 'Position', 'Get');";
    
    rc = sqlite3_exec(db, sql, 0, 0, &err_msg);
    
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
        sqlite3_close(db);
        
        return 1;
    }
    
    
    return 0;
}


int selectFunc(int rc, char *err_msg, sqlite3 *db) {
    char *sql = "SELECT * FROM emp";
    
    rc = sqlite3_exec(db, sql, callback, 0, &err_msg);
    
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "Failed to select data\n");
        fprintf(stderr, "SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
        sqlite3_close(db);
        
        return 1;
    }
    
    return 0;
}

int callback(void *NotUsed, int argc, char **argv,
             char **azColName) {
    
    NotUsed = 0;
    
    for (int i = 0; i < argc; i++) {
        
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    
    printf("\n");
    
    return 0;
}
