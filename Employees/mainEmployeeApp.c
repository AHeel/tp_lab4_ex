#include <sqlite3.h>
#include <stdio.h>
#include <string.h>


#define OK       0
#define NO_INPUT 1
#define TOO_LONG 2


static int getLine (char *prmpt, char *buff, size_t sz);
int selectorMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res);
int generalMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res);
int insertMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res);
int insertFunc(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res);
int deleteFunc(int rc, char *err_msg, sqlite3 *db);
int readImageFunc(int rc, char *err_msg, sqlite3 *db);
int selectByParams(int rc, char *err_msg, sqlite3 *db, char *param, sqlite3_stmt *res);
int callback(void *NotUsed, int argc, char **argv, char **azColName);
int insertImageFunc(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res);

//By default autocommit so don't need to change it
//if we need transactions
// "BEGIN TRANSACTION;"
//...
//"COMMIT;"

int main(void) {
    
    //set db connection
    sqlite3 *db;
    sqlite3_stmt *res;
    
    char *err_msg = 0;
    int rc = sqlite3_open("employees.db", &db);
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n",
                sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return 1;
    }
    
    rc = sqlite3_prepare_v2(db, "SELECT SQLITE_VERSION()", -1, &res, 0);
    
    
    //General menu
    int exit = 0;
    while(exit == 0) {
        exit = generalMenu(rc, err_msg, db, res);
    }
    
    
    //close db connection
    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}


//General menu
int generalMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res)
{
    char ch;
    int exit = 0;
    
    printf ("%s\n", "S - Execute SELECT");
    printf ("%s\n", "I - Execute INSERT");
    printf ("%s\n", "D - Execute DELETE");
    printf ("%s\n", "P - Out photo in file");
    printf ("%s\n", "E - Exit");
    
    scanf(" %c", &ch);
    
    printf ("%s\n", "");
    
    switch(ch) {

            
        case 'S': {
            printf ("%s\n", "Select");
            selectorMenu(rc, err_msg, db, res);
        }
            break;
            
        case 'I': {
            printf ("%s\n", "Insert");
            insertMenu(rc, err_msg, db, res);
        }
            break;
            
        case 'D': {
            printf ("%s\n", "Delete");
            deleteFunc(rc, err_msg, db);
        }
            break;
            
        case 'P': {
            printf ("%s\n", "P - Out photo in file");
            readImageFunc(rc, err_msg, db);
        }
            break;
            
        case 'E': {
            printf ("%s\n", "Exit");
            exit = 1;
        }
            break;
    }
    getchar();
    printf ("%s\n", "");
    
    
    if(exit == 1)
        return 1;
    else
        return 0;
}


int selectorMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res)
{
    char criterion[40];
    printf("%s\n", "Choose criterion for SELECT: ID, LASTNAME, COUNTRY: ");
    scanf("%s", criterion);
    
    if(strcmp("ID", criterion) == 0) {
        selectByParams(rc, err_msg, db, "ID", res);
    }
    
    if(strcmp("LASTNAME", criterion) == 0) {
        selectByParams(rc, err_msg, db, "LASTNAME", res);
    }
    
    if(strcmp("COUNTRY", criterion) == 0) {
        selectByParams(rc, err_msg, db, "COUNTRY", res);
    }


    return 1;
}


//Inesrt menu
int insertMenu(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res) {
    insertFunc(rc, err_msg, db, res);
    //insertImageFunc(rc, err_msg, db, res);  //debug functions
    return 1;
}


int insertFunc(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res) {
        const char insert_sql[] = "INSERT INTO emp (FIRSTNAME, LASTNAME, BIRTHDATA, PHOTO, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA) VALUES (?,?,?,?,?,?,?,?,?,?)";
    
    rc = sqlite3_prepare_v2(db, insert_sql, -1, &res, NULL);
    if(SQLITE_OK != rc) {
        fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", insert_sql, rc, sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
    }
    
    char *param_list[10];
    param_list[0] = "FIRSTNAME";
    param_list[1] = "LASTNAME";
    param_list[2] = "BIRTHDATA";
    param_list[3] = "PHOTO";
    param_list[4] = "CITY";
    param_list[5] = "COUNTRY";
    param_list[6] = "ADRESS";
    param_list[7] = "DEPARTMENT";
    param_list[8] = "POSITION";
    param_list[9] = "GETJOBDATA";

    // The NULL is "Don't a ttempt to free() the value when it's bound", since it's on the stack here
    for(int i = 1; i < 11; i++) {
        char *injectionattack = malloc(sizeof(char) * 10);
        printf(" %s:\n", param_list[i-1]);
        scanf(" %s", injectionattack);
        
        if(i == 4) {
            FILE *fp = fopen(injectionattack, "rb");
            if (fp == NULL) {
                
                fprintf(stderr, "Cannot open image file\n");
                
                return 1;
            }
            
            fseek(fp, 0, SEEK_END);
            if (ferror(fp)) {
                
                fprintf(stderr, "fseek() failed\n");
                int r = fclose(fp);
                
                if (r == EOF) {
                    fprintf(stderr, "Cannot close file handler\n");
                }
                
                return 1;
            }
            
            int flen = ftell(fp);
            if (flen == -1) {
                perror("error occurred");
                int r = fclose(fp);
                
                if (r == EOF) {
                    fprintf(stderr, "Cannot close file handler\n");
                }
                
                return 1;
            }
            
            fseek(fp, 0, SEEK_SET);
            if (ferror(fp)) {
                fprintf(stderr, "fseek() failed\n");
                int r = fclose(fp);
                
                if (r == EOF) {
                    fprintf(stderr, "Cannot close file handler\n");
                }
                
                return 1;
            }
            
            char data[flen+1];
            int size = fread(data, 1, flen, fp);
            if (ferror(fp)) {
                fprintf(stderr, "fread() failed\n");
                int r = fclose(fp);
                
                if (r == EOF) {
                    fprintf(stderr, "Cannot close file handler\n");
                }
                
                return 1;     
            }
            
            int r = fclose(fp);
            if (r == EOF) {
                fprintf(stderr, "Cannot close file handler\n");
            }
            
            rc = sqlite3_bind_blob(res, i, data, size, NULL);
            if(SQLITE_OK != rc) {
                fprintf(stderr, "Error binding value in insert (%i): %s\n", rc, sqlite3_errmsg(db));
                sqlite3_close(db);
                exit(1);
            } else {
                //printf("Successfully bound string for insert: '%s'\n", injectionattack);
            }
            
            continue;
        }

        rc = sqlite3_bind_text(res, i, injectionattack, sizeof(injectionattack), NULL);
        if(SQLITE_OK != rc) {
            fprintf(stderr, "Error binding value in insert (%i): %s\n", rc, sqlite3_errmsg(db));
            sqlite3_close(db);
            exit(1);
        } else {
            //printf("Successfully bound string for insert: '%s'\n", injectionattack);
        }
    }

    rc = sqlite3_step(res);
    if(SQLITE_DONE != rc) {
        fprintf(stderr, "insert statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
    } else {
        //printf("INSERT completed\n\n");
    }
    
    return 0;
}


int insertImageFunc(int rc, char *err_msg, sqlite3 *db, sqlite3_stmt *res){
    FILE *fp = fopen("Kan.jpg", "rb");
    
    if (fp == NULL) {
        
        fprintf(stderr, "Cannot open image file\n");
        
        return 1;
    }
    
    fseek(fp, 0, SEEK_END);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    int flen = ftell(fp);
    
    if (flen == -1) {
        
        perror("error occurred");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    fseek(fp, 0, SEEK_SET);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    char data[flen+1];
    
    int size = fread(data, 1, flen, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fread() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return 1;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    
    char *sql = "INSERT INTO emp (PHOTO) VALUES(?)";
    
    rc = sqlite3_prepare(db, sql, -1, &res, 0);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot prepare statement: %s\n", sqlite3_errmsg(db));
        
        return 1;
    }
    
    sqlite3_bind_blob(res, 1, data, size, SQLITE_STATIC);
    rc = sqlite3_step(res);
    
    if (rc != SQLITE_DONE) {
        
        printf("execution failed: %s", sqlite3_errmsg(db));
    }
    
    return 0;
}


//For reading from console
static int getLine (char *prmpt, char *buff, size_t sz) {
    int ch, extra;
    
    // Get line with buffer overrun protection.
    if (prmpt != NULL) {
        printf ("%s", prmpt);
        fflush (stdout);
    }
    if (fgets (buff, sz, stdin) == NULL)
        return NO_INPUT;
    
    // If it was too long, there'll be no newline. In that case, we flush
    // to end of line so that excess doesn't affect the next call.
    if (buff[strlen(buff)-1] != '\n') {
        extra = 0;
        while (((ch = getchar()) != '\n') && (ch != EOF))
            extra = 1;
        return (extra == 1) ? TOO_LONG : OK;
    }
    
    // Otherwise remove newline and give string back to caller.
    buff[strlen(buff)-1] = '\0';
    return OK;
}


//Delete
int deleteFunc(int rc, char *err_msg, sqlite3 *db){
    
    char choosenName[40];
    getchar();
    getLine("Choose firstname: ", choosenName, sizeof(choosenName));
    
    char *buff = "DELETE FROM emp WHERE FIRSTNAME = '";
    char sql[40];
    strcpy(sql, buff);
    strcat(sql, choosenName);
    strcat(sql, "'");

    printf("%s", sql);
    
    rc = sqlite3_exec(db, sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
        sqlite3_close(db);
        
        return 1;
    }
    
    return 0;
}


//read image from sqlite3 and write to file
int readImageFunc(int rc, char *err_msg, sqlite3 *db){
    char filename[40];
    getchar();
    getLine("Input filename: ", filename, sizeof(filename));
    
    FILE *fp = fopen(filename, "wb");
    
    if (fp == NULL) {
        
        fprintf(stderr, "Cannot open image file\n");
        
        return 1;
    }
    
    char choosenName[40];
    getLine("Choose firstname: ", choosenName, sizeof(choosenName));
    char *buff = "SELECT PHOTO FROM emp WHERE FIRSTNAME = '";
    char sql[80];
    strcpy(sql, buff);
    strcat(sql, choosenName);
    strcat(sql, "'");
    
    sqlite3_stmt *pStmt;
    rc = sqlite3_prepare_v2(db, sql, -1, &pStmt, 0);
    
    if (rc != SQLITE_OK ) {
        
        fprintf(stderr, "Failed to prepare statement\n");
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        
        sqlite3_close(db);
        
        return 1;
    }
    
    rc = sqlite3_step(pStmt);
    
    int bytes = 0;
    
    if (rc == SQLITE_ROW) {
        
        bytes = sqlite3_column_bytes(pStmt, 0);
    }
    
    fwrite(sqlite3_column_blob(pStmt, 0), bytes, 1, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fwrite() failed\n");
        
        return 1;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    rc = sqlite3_finalize(pStmt);
    
    return 0;
}


//Select by params
int selectByParams(int rc, char *err_msg, sqlite3 *db, char *param, sqlite3_stmt *res){
    int intParam = 1;
    char sqlReq[40];

    if(strcmp("ID", param) == 0) {
        printf("%s", "Input ID: ");
        scanf("%d", &intParam);
        
        char *sql = "SELECT ID, FIRSTNAME, LASTNAME, BIRTHDATA, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA FROM emp WHERE ID = @ID";
        
        rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
        
        if (rc == SQLITE_OK) {
            int idx = sqlite3_bind_parameter_index(res, "@ID");
            sqlite3_bind_int(res, idx, intParam);
        } else {
            fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
        }
    }
    
    if(strcmp("LASTNAME", param) == 0) {
        printf("%s", "Input LASTNAME: ");
        scanf("%s", sqlReq);
        
        char *sql = "SELECT ID, FIRSTNAME, LASTNAME, BIRTHDATA, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA FROM emp WHERE LASTNAME = @LASTNAME";
        
        rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
        
        if (rc == SQLITE_OK) {
            int idx = sqlite3_bind_parameter_index(res, "@LASTNAME");
            rc = sqlite3_bind_text(res, idx, sqlReq, -1, SQLITE_STATIC);
        } else {
            fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
        }
    }
    
    if(strcmp("COUNTRY", param) == 0) {
        printf("%s", "Input COUNTRY: ");
        scanf("%s", sqlReq);
        
        char *sql = "SELECT ID, FIRSTNAME, LASTNAME, BIRTHDATA, CITY, COUNTRY, ADRESS, DEPARTMENT, POSITION, GETJOBDATA FROM emp WHERE COUNTRY = @CONTRY";
        
        rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
        
        if (rc == SQLITE_OK) {
            int idx = sqlite3_bind_parameter_index(res, "@CONTRY");
            rc = sqlite3_bind_text(res, idx, sqlReq, -1, callback);
        } else {
            fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
        }
    }
    
    
    int step = sqlite3_step(res);
    
    if (step == SQLITE_ROW) {
        printf("%s\n", sqlite3_column_text(res, 0));
        printf("%s\n", sqlite3_column_text(res, 1));
        printf("%s\n", sqlite3_column_text(res, 2));
        printf("%s\n", sqlite3_column_text(res, 3));
        printf("%s\n", sqlite3_column_text(res, 5));
        printf("%s\n", sqlite3_column_text(res, 6));
        printf("%s\n", sqlite3_column_text(res, 7));
        printf("%s\n", sqlite3_column_text(res, 8));
        printf("%s\n", sqlite3_column_text(res, 9));
    }

    return 0;
}


int callback(void *NotUsed, int argc, char **argv, char **azColName) {
    
    NotUsed = 0;
    
    for (int i = 0; i < argc; i++) {
        
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    
    printf("\n");
    
    return 0;
}
